import os
import sys
args = sys.argv
directory = args[1]
protoc_path = args[2]
for file in os.listdir(directory):
    if file.endswith(".proto"):
        os.system(protoc_path+" "+directory+"/"+file+" --python_out=.")


'''
# original cmd
    > ./bin/protoc object_detection/protos/*.proto --python_out=. 

# use this to run script
    > python use_protobuf.py object_detection/protos C:/Users/Gilbert/Downloads/bin/protoc

'''
