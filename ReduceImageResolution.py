import PIL
from PIL import Image
import os
from tqdm import tqdm
import sys

IMAGE_SIZE = 100

# Set Directories Path
INPUT_DIR = os.getcwd()+"\Test"
try:
    INPUT_DIR = os.getcwd()+sys.argv[1]
except Exception as e:
    print("Current Directory set as INPUT_DIR ..")
    pass

OUTPUT_DIR = INPUT_DIR + "\Output\\"+("{}x{}".format(IMAGE_SIZE, IMAGE_SIZE))
os.makedirs(OUTPUT_DIR, exist_ok=True)

print(INPUT_DIR, OUTPUT_DIR)

cnt = 0
for image in tqdm(os.listdir(INPUT_DIR)):
    try:
        im = Image.open(os.path.join(INPUT_DIR, image))
        im = im.resize((IMAGE_SIZE, IMAGE_SIZE), PIL.Image.ANTIALIAS)
        # new_img =
        cnt = cnt+1
        im.save(os.path.join(OUTPUT_DIR, "Sample_{}.jpg".format(cnt)),
                'JPEG', quality=90)
    except Exception as e:
        print("Exception is ", e)
        pass

print("Resize images are saved in \"{0}\" directory.".format(OUTPUT_DIR))
