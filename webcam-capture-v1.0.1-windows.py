import cv2
from time import sleep
import os

key = cv2. waitKey(1)
webcam = cv2.VideoCapture(0)
sleep(2)

'''
while True:

    try:
        check, frame = webcam.read()
        print(check) #prints true as long as the webcam is running
        print(frame) #prints matrix values of each framecd 
        cv2.imshow("Capturing", frame)
        key = cv2.waitKey(1)
        if key == ord('s'): 
            cv2.imwrite(filename='saved_img.jpg', img=frame)
            print("Image saved!")                   
        elif key == ord('q'):
            webcam.release()
            cv2.destroyAllWindows()
            break
    
    except(KeyboardInterrupt):
        print("Turning off camera.")
        webcam.release()
        print("Camera off.")
        print("Program ended.")
        cv2.destroyAllWindows()
        break
'''


def startCapturing(save_directory,sleep_time):
    cnt=0
    os.makedirs(save_directory, exist_ok=True)
    while True:
        
        try:
            check, frame = webcam.read()
            cv2.imshow("Capturing", frame)
            key = cv2.waitKey(1)

            cnt=cnt+1
            file_name="Sample_{}.jpg".format(cnt) 
            cv2.imwrite(filename=save_directory+"/"+file_name, img=frame)
            print(file_name+" saved!")   

            sleep(sleep_time)           
        except Exception as e:
            print(e)
            webcam.release()
            print("Camera off.")
            print("Program ended.")
            cv2.destroyAllWindows()
            break

startCapturing("images/test3",2)